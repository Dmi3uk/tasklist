<%--
  Created by IntelliJ IDEA.
  User: Tony
  Date: 02.11.2019
  Time: 8:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>SearchTask</title>
</head>
<body>
<h3>SEARCH TASKS</h3>

<form action="/add">
    <button type="submit">Add new Task</button>
</form>

    <form method="post">
        <label>StartDate:</label><br>
        <input name="startDate"/><br><br>
        <label>EndDate:</label><br>
        <input name="endDate"/><br><br>
        <label>Assignee</label>
        <select name="assignee">
            <option value="">All assignees</option>
            <c:forEach var="assignee" items="${assignees}">
                <option>${assignee.name} </option>
            </c:forEach>
            </select>
        <br><br>
        <label>Period</label>
        <select name="period">
            <option>Last Quarter</option>
            <option>Last Month</option>
            <option>Last Week</option>
            <option>Carrent Quarter to Date</option>
            <option>Carrent Month to Date</option>
            <option>Carrent Week to Date</option>
        </select>
        <br><br>


        <input type="submit" value="Find"/><br><br>



        <table border="2">
            <tr>
                <td>Id</td>
                <td>Summary</td>
                <td>StartDate</td>
                <td>EndDate</td>
                <td>Assignee</td>
            </tr>

            <c:forEach var="task" items="${tasks}">
                <tr>
                    <td>${task.id}</td>
                    <td>${task.summary}</td>
                    <td>${task.startDate}</td>
                    <td>${task.endDate}</td>
                    <td>${task.assignee}</td>

                </tr>
            </c:forEach>
        </table>
    </form>
</body>
</html>
