package connectors;

import java.sql.Connection;
import java.sql.DriverManager;


public abstract class MysqlDbConnection {
    public static Connection getDbConnection() {
        Connection connection = null;
        String url = "jdbc:mysql://localhost/tasks?serverTimezone=Europe/Minsk&useSSL=false";
        String pass = "root";
        String user = "root";

        try {
            Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
            connection = DriverManager.getConnection(url, user, pass);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return connection;
    }
}

