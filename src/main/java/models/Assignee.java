package models;

public class Assignee {
    private  String name;

    public Assignee(){

    }

    public Assignee(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
