package servlets;

import dao.TaskDAO;
import models.Task;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;


@WebServlet("/add")
public class AddTaskServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        getServletContext().getRequestDispatcher("/WEB-INF/views/AddTask.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            String summary = req.getParameter("summary");
            LocalDate startDate = LocalDate.parse(req.getParameter("startDate"), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            LocalDate endDate = LocalDate.parse(req.getParameter("endDate"), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            String assignee = req.getParameter("assignee");
            Task task = new Task(summary, startDate, endDate, assignee);
            TaskDAO.insert(task);
            resp.sendRedirect("list");
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }
}