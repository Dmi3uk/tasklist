package servlets;

import dao.TaskDAO;
import models.Assignee;
import models.Period;
import models.Task;
import others.SelectionPeriod;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

@WebServlet("/")
public class ListTaskServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        ArrayList<Task> tasks = TaskDAO.getTasks();
        req.setAttribute("tasks", tasks);
        ArrayList<Assignee> assignees = TaskDAO.assignees();
        req.setAttribute("assignees", assignees);
        getServletContext().getRequestDispatcher("/WEB-INF/views/SearchTasks.jsp").forward(req, resp);
    }
}
