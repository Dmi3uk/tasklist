package dao;

import connectors.MysqlDbConnection;
import models.Assignee;
import models.Task;
import java.sql.*;
import java.util.ArrayList;

public abstract class TaskDAO {


    public static ArrayList<Task> getTasks() {
        ArrayList<Task> tasks = new ArrayList<>();
        String sql = "SELECT * FROM tasks;";
        Connection connection = MysqlDbConnection.getDbConnection();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                Task task = new Task();
                task.setId(resultSet.getInt("id"));
                task.setSummary(resultSet.getString("summary"));
                task.setStartDate(resultSet.getDate("start_date").toLocalDate());
                task.setEndDate(resultSet.getDate("end_date").toLocalDate());
                task.setAssignee(resultSet.getString("assignee"));
                tasks.add(task);
            }
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return tasks;
    }


    public static ArrayList<Assignee> assignees() {
        ArrayList<Assignee> assignees = new ArrayList<>();
        String sql = "SELECT DISTINCT assignee FROM tasks;";
        Connection connection = MysqlDbConnection.getDbConnection();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                Assignee assignee = new Assignee();
                assignee.setName(resultSet.getString("assignee"));
                assignees.add(assignee);
            }
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return assignees;
    }


    public static int insert(Task task) {
        try {
            try (Connection connection = MysqlDbConnection.getDbConnection()) {

                String sql = "INSERT INTO tasks (summary, start_date, end_date, assignee) Values (?, ?, ?, ?)";
                try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                    preparedStatement.setString(1, task.getSummary());
                    preparedStatement.setDate(2, Date.valueOf(task.getStartDate()));
                    preparedStatement.setDate(3, Date.valueOf(task.getEndDate()));
                    preparedStatement.setString(4, task.getAssignee());

                    return preparedStatement.executeUpdate();
                }
            }
        } catch (Exception ex) {
            System.out.println(ex);
        }
        return 0;
    }
}
