package others;

import models.Period;

import java.time.LocalDate;

public abstract class SelectionPeriod {

    private static Period period;
    private static LocalDate firstQuarter = LocalDate.of(2019, 1, 1);
    private static LocalDate secondQuarter = LocalDate.of(2019, 4, 1);
    private static LocalDate thirdQuarter = LocalDate.of(2019, 7, 1);
    private static LocalDate fourthQuarter = LocalDate.of(2019, 10, 1);
    private static LocalDate newYear = LocalDate.of(2020, 1, 1);

    public static Period lastQuarter() {
        if (LocalDate.now().isAfter(firstQuarter) && LocalDate.now().isBefore(secondQuarter)) {
            period = new Period(firstQuarter, secondQuarter);
        } else if (LocalDate.now().isAfter(secondQuarter) && LocalDate.now().isBefore(thirdQuarter)) {
            period = new Period(secondQuarter, thirdQuarter);
        } else if (LocalDate.now().isAfter(thirdQuarter) && LocalDate.now().isBefore(fourthQuarter)) {
            period = new Period(thirdQuarter, fourthQuarter);
        } else {
            period = new Period(fourthQuarter, newYear);
        }
        return period;
    }

    public static Period lastMonth() {
        LocalDate startData = LocalDate.of(2019, LocalDate.now().getMonthValue(), 1);
        LocalDate endData = startData.plusMonths(1);
        period = new Period(startData, endData);
        return period;
    }

    public static Period lastWeek() {
//        TODO
        return period;
    }

    public static Period CurrentQuarterToDate() {
        if (LocalDate.now().isAfter(firstQuarter) && LocalDate.now().isBefore(secondQuarter)) {
            period = new Period(firstQuarter, LocalDate.now());
        } else if (LocalDate.now().isAfter(secondQuarter) && LocalDate.now().isBefore(thirdQuarter)) {
            period = new Period(secondQuarter, LocalDate.now());
        } else if (LocalDate.now().isAfter(thirdQuarter) && LocalDate.now().isBefore(fourthQuarter)) {
            period = new Period(thirdQuarter, LocalDate.now());
        } else {
            period = new Period(fourthQuarter, LocalDate.now());
        }
        return period;
    }

    public static Period CurrentMonthToDate() {
        LocalDate startData = LocalDate.of(2019, LocalDate.now().getMonthValue(), 1);
        LocalDate endData = LocalDate.now();
        period = new Period(startData, endData);
        return period;
    }

    public static Period CurrentWeekToDate() {
//       TODO
        return period;
    }

}
